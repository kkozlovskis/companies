<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Demo User',
            'email' => 'admin@admin.lv ',
            'password' => bcrypt('parole'),
        ]);
        factory(App\Company::class, 30)->create();
    }
}
