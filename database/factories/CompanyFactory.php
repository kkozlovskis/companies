<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => str_random(30),
        'website' => 'https://' . str_random(10) . '.com/' . str_random(6),
        'email' => $faker->unique()->safeEmail,
    ];
});
