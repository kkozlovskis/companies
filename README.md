# Companies

## Installation

After repo is cloned do following:

Let's install all packages:

```
composer install
```

Copy .env.example file to .env file:

```
cp .env.example .env
```

After that create new local DB and add vhost for application and DocumentRoot  to /public folder. 
After that edit .env file with your created DB.
Then run following comands.

Generates app key:

```
php artisan key:generate
```

Create application tables:

```
php artisan migrate
```

Seed database with data and creates user admin@admin.lv with password "parole"

```
php artisan db:seed
```

Now, application is ready to use!