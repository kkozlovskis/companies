@extends('layouts.app')

@section('content')
    <div class="uper">
        <div style="padding-left: 20px">
            <a href="{{ route('companies.create')}}" class="btn btn-success">Create new company</a>
        </div><br />
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Name</td>
                <td>Address</td>
                <td>Website</td>
                <td>Email</td>
                <td colspan="2">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    <td>{{$company->address}}</td>
                    <td>{{$company->website}}</td>
                    <td>{{$company->email}}</td>
                    <td><a href="{{ route('companies.edit',$company->id)}}" class="btn btn-primary">Edit</a></td>
                    <td>
                        <form action="{{ route('companies.destroy', $company->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {!! $companies->links() !!}
        </div>
    </div>
@endsection