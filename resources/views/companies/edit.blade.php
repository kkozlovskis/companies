@extends('layouts.app')

@section('content')
    <div class="card uper">
        <div class="card-header">
            Edit company
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('companies.update', $company->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Company name:</label>
                    <input type="text" class="form-control" name="company_name" value={{ $company->name}} />
                </div>
                <div class="form-group">
                    <label for="address">Company address:</label>
                    <input type="text" class="form-control" name="company_address" value={{ $company->address}} />
                </div>
                <div class="form-group">
                    <label for="website">Company website:</label>
                    <input type="text" class="form-control" name="company_website" value={{ $company->website}} />
                </div>
                <div class="form-group">
                    <label for="email">Company email:</label>
                    <input type="text" class="form-control" name="company_email" value={{ $company->email}} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection