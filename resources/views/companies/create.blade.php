@extends('layouts.app')

@section('content')
    <div class="card uper">
        <div class="card-header">
            Add company
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('companies.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="name">Company Name:</label>
                    <input type="text" class="form-control" name="company_name"/>
                </div>
                <div class="form-group">
                    <label for="address">Company address:</label>
                    <input type="text" class="form-control" name="company_address"/>
                </div>
                <div class="form-group">
                    <label for="website">Company website:</label>
                    <input type="text" class="form-control" name="company_website"/>
                </div>
                <div class="form-group">
                    <label for="email">Company email:</label>
                    <input type="text" class="form-control" name="company_email"/>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
@endsection