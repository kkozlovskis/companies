<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Http\Requests\CompaniesStoreRequest;

class CompaniesController extends Controller
{

    public function index()
    {
        $companies = Company::paginate(10);

        return view('companies.index', compact('companies'));
    }

    public function create()
    {
        return view('companies.create');
    }

    public function store(CompaniesStoreRequest $request)
    {
        $company = new Company([
            'name' => $request->get('company_name'),
            'address' => $request->get('company_address'),
            'website' => $request->get('company_website'),
            'email' => $request->get('company_email')
        ]);
        $company->save();
        return redirect('/companies')->with('success', 'Company added');
    }

    public function edit($id)
    {
        $company = Company::find($id);

        return view('companies.edit', compact('company'));

    }

    public function update(CompaniesStoreRequest $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request->get('company_name');
        $company->address = $request->get('company_address');
        $company->website = $request->get('company_website');
        $company->email = $request->get('company_email');
        $company->save();

        return redirect('/companies')->with('success', 'Company updated');

    }

    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();

        return redirect('/companies')->with('success', 'Company has been deleted');
    }
}
